from pandas import read_json, read_csv, DataFrame
from pandas.io.json import json_normalize
import requests

def dataPull(maxRecords, gender, WOD, ageDiv):
    # Build URL for Airtable API
    url = "https://api.airtable.com/v0/app62FHxeCDHOvSm6/WOD?" 
    header= {"Authorization": "Bearer keyl0isPkjkj5G5Wr"}
    parameters = {"maxRecords": maxRecords, "filterByFormula": f'AND({{Gender}} = "{gender}", {{WOD}} = "{WOD}", {{Age Div}} = "{ageDiv}")', "fields": ["Result", "RX or Scaled", "Name", "Age Div"], "cellFormat": "string", "timeZone": "America/Chicago", "userLocale": "en-gb", "sort[0][field]":"Result", "sort[0][direction]": "asc"}

    # retrieve api request
    r = requests.get(url, headers=header, params=parameters)
    data = r.json()

    # delete unwanted fields
    for el in data['records']:
        del el['id']
        del el['createdTime']

    # normalize JSON data to flat table
    df = json_normalize(data['records'])

    # remove fields part of labels in columns and reorder
    df = df.rename(lambda x: x[7:], axis='columns')
    df = df[['Name', 'Result', 'RX or Scaled']]
    df = df.rename(columns={'Result': WOD})

    # modify vars for safe file names
    if "<" in ageDiv:
        ageDivSafe = "under37"
    else:
        ageDivSafe = "37plus"
        
    # convert JSON data to HTML table and output
    df.to_html(
            f'html/{gender.lower()}-{ageDivSafe}.html',
            classes='dataframe table table-striped table-bordered display dtresponsive no-wrap',
            border=0,
            justify='center',
            index=False,
            table_id=f"leaderboard-{gender.lower()}-{ageDivSafe}")

if __name__ == "__main__":
    dataPull(3, "Female", "19.1","<37")
    dataPull(3, "Male", "19.1","37+")
